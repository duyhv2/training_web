Rails.application.routes.draw do
  resources :examples
  devise_for :users, path: 'users', controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }

  devise_for :admins, path: 'admins', controllers: {
    sessions: 'admins/sessions'
  }

  devise_scope :user do
    namespace :users do
      get 'dashboards/index', as: :root
    end
  end

  devise_scope :admin do
    namespace :admins do
      get 'dashboards/index', as: :root
      resources :users, only: [:index, :destroy, :edit, :update]
      resources :questions
    end
  end

  root to: 'users/dashboards#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
