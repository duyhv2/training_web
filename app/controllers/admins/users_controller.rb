class Admins::UsersController < Admins::AdminsController
    before_action :set_user, only: [:update, :edit, :destroy]
    def index
        @users = User.all.page(params[:page]).per(6)
    end

    def edit
    end

    def update
        if @user.update(user_params)
            flash[:success] = "User successfully updated"
        else
            flash[:danger] = "User not updated"
        end
        redirect_to edit_admins_user_path(@user)
    end

    def destroy
        if @user.destroy
            flash[:notice] = "User has been deleted"
        else
            flash[:notice] = "Delete user not allowed"
        end
        redirect_to admins_users_path
    end

    private
        def set_user
            @user = User.find(params[:id])
        end

        def user_params
            params.require(:user).permit(:email, :password, :password_confirmation)
        end
end
