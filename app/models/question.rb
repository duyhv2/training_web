class Question < ApplicationRecord
    has_many :answers, dependent: :destroy
    accepts_nested_attributes_for :answers
    validates :content, presence: true

    enum category: {Art: 0, Geography: 1, English: 2, Literacy: 3, Music: 4, Science: 5}
    enum level: {Easy: 0, Normal: 1, Hard: 2}
end
