class Admins::QuestionsController < Admins::AdminsController
    before_action :set_question, only: [:edit, :show, :destroy, :update]

    def index
        @search = Question.ransack(params[:q])
        @questions = @search.result.page(params[:page]).per(10)
    end

    def edit
    end

    def new
        @question = Question.new
        (1..4).each { @question.answers.build }
    end

    def update
        respond_to do |format|
            if @question.update(question_params)
                format.html { redirect_to admins_questions_path(@question), notice: 'Question was successfully updated.' }
            else
                flash[:error] = "An error has occurred while"
                format.html { render :edit, error: 'Error updating question'}
            end
        end
    end

    def create
        @question = Question.new(question_params)
        respond_to do |format|
            if @question.save
                format.html { redirect_to new_admins_question_path, notice: 'Question was successfully created'}
            else
                flash[:error] = @question.errors.full_messages.first.gsub!(/[^a-zA-Z ]/,'')
                format.html { redirect_to new_admins_question_path }
            end
        end
    end

    def destroy
        if @question.destroy
            flash[:success] = "Question was successfully destroyed"
        else
            flash[:danger] = "Question was not destroyed"
        end
        redirect_to  admins_questions_path
    end
    
    private
        def set_question
            @question = Question.find(params[:id])
        end

        def question_params
            params.require(:question).permit(:content, :category, :type, :level, answers_attributes: [:content, :correct])
        end
end
